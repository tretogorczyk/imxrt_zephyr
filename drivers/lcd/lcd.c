#include <zephyr.h>
#include <misc/printk.h>
#include <lcd.h>

#include <device.h>
#include <gpio.h>
#include <soc.h>
#include <fsl_common.h>

static int lcd_init(struct device *dev)
{
	printk("inside lcd drv...\n");
//	IRQ_CONNECT(LCDIF_BASE + LCDIF_CTRL1_CLR_CUR_FRAME_DONE_IRQ, 0,
//		    mcux_igpio_port_isr, DEVICE_GET(lcd_dev), 0);
//
//	irq_enable(LCDIF_CTRL1_CLR_CUR_FRAME_DONE_IRQ);


	return 0;
}

struct lcd_config {
};

struct lcd_data {
	/* port ISR callback routine address */
//	sys_slist_t callbacks;
	/* pin callback routine enable flags, by pin number */
//	u32_t pin_callback_enables;
};


static const struct lcd_config lcd_dev_config = {
};

static struct lcd_data lcd_dev_data;


//void APP_LCDIF_IRQHandler(void)
//{
//	printk("inside irq handler for LCDIF\n");
//    uint32_t intStatus;
//
//    intStatus = ELCDIF_GetInterruptStatus(APP_ELCDIF);
//
//    ELCDIF_ClearInterruptStatus(APP_ELCDIF, intStatus);
//
//    if (intStatus & kELCDIF_CurFrameDone)
//    {
//        s_frameDone = true;
//    }
///* Add for ARM errata 838869, affects Cortex-M4, Cortex-M4F Store immediate overlapping
//  exception return operation might vector to incorrect interrupt */
//#if defined __CORTEX_M && (__CORTEX_M == 4U)
//    __DSB();
//#endif
//}


DEVICE_AND_API_INIT(lcd_dev, "LCD_DEV",
					lcd_init,
					&lcd_dev_data, &lcd_dev_config,
					POST_KERNEL, CONFIG_KERNEL_INIT_PRIORITY_DEFAULT,
					NULL);
