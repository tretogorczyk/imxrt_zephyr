/*
 * Copyright (c) 2018 Arrow Electronics, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _ARROW_IMAGELIB_H_
#define _ARROW_IMAGELIB_H_

/* Frame buffer data alignment, for better performance, the LCDIF frame buffer should be 64B align. */
#define FRAME_BUFFER_ALIGN 64

#define APP_IMG_HEIGHT 272
#define APP_IMG_WIDTH 480

enum imagelib_image_id {
	IMAGELIB_IMAGE_ARROW_LOGO 	= 0,
	IMAGELIB_IMAGE_NXP_LOGO 	= 1,
	IMAGELIB_IMAGE_SAMPLE_LOGO 	= 2,
	IMAGELIB_IMAGE_LAST
};

/*
 * @brief: Copies stored image to the framebuffer
 *
 * @param img_id 		- id of the image to copy over
 * @param frame_buffer	- handle to the frame buffer
 *
 * @retval 0 on success, negative error codes on failure
 */
int imagelib_draw_logo(enum imagelib_image_id img_id, uint32_t frame_buffer[APP_IMG_HEIGHT][APP_IMG_WIDTH]);

#endif /* _ARROW_IMAGELIB_H_ */
