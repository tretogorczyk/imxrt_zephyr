/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <ztest.h>

#include "../../../../../../apps/neural_net_101/src/neural_network.c"
#include "../../../../../../apps/neural_net_101/src/neural_network.h"

static void init_tests(void)
{
	zassert_equal(nn_init(5), 0, "incorrect status code for first execution of init call");
	zassert_equal(nn_init(5), -EIO, "incorrect status code for another execution of init call");
}

static void test_sigmoid()
{
	zassert_equal(0.5f, nn_calc_sigmoid(0.0f, false), "incorrect sigmoid value");
	zassert_equal(0.75f, nn_calc_sigmoid(1.0f, false), "incorrect sigmoid value");
	zassert_true(0.833333f + 0.0001 > nn_calc_sigmoid(2.0f, false), "incorrect sigmoid value");
	zassert_true(0.833333f - 0.0001 < nn_calc_sigmoid(2.0f, false), "incorrect sigmoid value");

	zassert_true(0.1666667f + 0.0001 > nn_calc_sigmoid(-2.0f, false), "incorrect sigmoid value");
	zassert_true(0.1666667f - 0.0001 < nn_calc_sigmoid(-2.0f, false), "incorrect sigmoid value");

	zassert_true(0.25f + 0.0001 > nn_calc_sigmoid(-1.0f, false), "incorrect sigmoid value");
	zassert_true(0.25f - 0.0001 < nn_calc_sigmoid(-1.0f, false), "incorrect sigmoid value");
}

static void test_dot_product()
{
	float a[] = {2.0f, 4.0f, 8.0f};
	float b[] = {0.5f, 0.25f, 0.125f};

	zassert_equal(3.0f, nn_calc_dot_product(3, a, b), "incorrect dot product value");
	float c[] = {-0.25f, -0.4f, -2.0f};
	zassert_equal(-18.1f, nn_calc_dot_product(3, a, c), "incorrect dot product value");
}

static void test_predict()
{
	float output = 15.0f;
	float weights[3] = {34.62191f, -2.279318f, -14.5473f};
	const float inputs[] = {0.0f, 0.0f, 1.0f};
	zassert_equal(nn_init(3), 0, "incorrect status code for first execution of init call");
	ctx.layer_count = 0;
	zassert_equal(nn_predict(4, inputs, &output), -EIO, "no layers configured, should fail");
	zassert_equal(nn_add_layer(1), 0, "correct call should not fail");
	zassert_equal(nn_set_neuron(0, 0, 0, 3, weights), 0, "correct call, should return 0");
	zassert_equal(ctx.layers[0].weight_count, 3, "layer configuration is not correctly set");

	zassert_equal(nn_predict(4, inputs, &output), -EIO, "incorrect input to the function, should fail");
	zassert_equal(nn_predict(0, inputs, &output), -EIO, "incorrect input to the function, should fail");
	zassert_equal(nn_predict(-1, inputs, &output), -EIO, "incorrect input to the function, should fail");
	zassert_equal(nn_predict(3, NULL, &output), -ENOENT, "null ptr, should fail");
	zassert_equal(nn_predict(3, inputs, NULL), -ENOENT, "null ptr, should fail");


	zassert_equal(-14.5473f, nn_calc_dot_product(3, weights, inputs), "incorrect dot product value");
	zassert_true(0.03215992f + 0.0001f > nn_calc_sigmoid(-14.5473f, false), "incorrect sigmoid value");
	zassert_true(0.03215992f - 0.0001f < nn_calc_sigmoid(-14.5473f, false), "incorrect sigmoid value");

	nn_predict(3, inputs, &output);
	zassert_equal(ctx.layers[0].neurons[0].dot_product, -14.5473f, "incorrect dot product");
	zassert_true(ctx.layers[0].neurons[0].output < 0.03215992f + 0.0001f, "incorrect sigmoid fun output");
	zassert_true(ctx.layers[0].neurons[0].output > 0.03215992f - 0.0001f, "incorrect sigmoid fun output");

	zassert_true(output < 0.03215992f + 0.0001f, "incorrect sigmoid fun output");
	zassert_true(output > 0.03215992f - 0.0001f, "incorrect sigmoid fun output");

}


static void test_predict_two_hidden_layers_3_1()
{
	/*
	 * x1	|>
	 *
	 * x2 	|>   |> y
	 *
	 * x3 	|>
	 */
	float output = 15.0f;

	float weights_l0[3][3] = {{2.2241f, 0.37696f, -1.26952f}, {-2.56197f, 0.49292f, 0.26323f}, {2.6323f, 0.32058f, -0.31596f}};
	float offset_l0[3] = {-0.27114f, 0.56864f, 1.27678f};

	float weights_l1[3] = {3.89077f, -2.30283f, -2.99667f};
	float offset_l1 = 0.73619f;

	const float inputs[] = {0.0f, 0.0f, 1.0f};
	zassert_equal(nn_init(3), 0, "incorrect status code for first execution of init call");
	ctx.layer_count = 0;
	zassert_equal(nn_predict(4, inputs, &output), -EIO, "no layers configured, should fail");
	zassert_equal(nn_add_layer(3), 0, "correct call should not fail");

	int input_count = 3;
	int neuron_id;
	for (neuron_id = 0 ; neuron_id < 3 ; neuron_id++){
		zassert_equal(nn_set_neuron(0, neuron_id, offset_l0[neuron_id], 3, weights_l0[neuron_id]), 0, "correct call, should return 0");
		int weight_id;
		for(weight_id = 0; weight_id < input_count ; weight_id++){
			zassert_equal(ctx.layers[0].neurons[neuron_id].weights[weight_id], weights_l0[neuron_id][weight_id], "weights are not correctly set for neuron");
		}
		zassert_equal(ctx.layers[0].neurons[neuron_id].offset, offset_l0[neuron_id], "offset is not set correctly");
	}


	zassert_equal(nn_add_layer(1), 0, "correct call should not fail");
	zassert_equal(nn_set_neuron(1, 0, offset_l1, 3, weights_l1), 0, "correct call, should return 0");
	int weight_id;
	for(weight_id = 0; weight_id < input_count ; weight_id++){
		zassert_equal(ctx.layers[1].neurons[0].weights[weight_id], weights_l1[weight_id], "weights are not correctly set for neuron");
	}
	zassert_equal(ctx.layers[1].neurons[0].offset, offset_l1, "offset is not set correctly");
	zassert_equal(ctx.layers[0].weight_count, 3, "layer configuration is not correctly set");
	zassert_equal(nn_predict(4, inputs, &output), -EIO, "incorrect input to the function, should fail");
	zassert_equal(nn_predict(0, inputs, &output), -EIO, "incorrect input to the function, should fail");
	zassert_equal(nn_predict(-1, inputs, &output), -EIO, "incorrect input to the function, should fail");
	zassert_equal(nn_predict(3, NULL, &output), -ENOENT, "null ptr, should fail");
	zassert_equal(nn_predict(3, inputs, NULL), -ENOENT, "null ptr, should fail");


	nn_predict(3, inputs, &output);
//	printf("value of output: %2.9f\n", output);
	zassert_true(output < 0.146846071f + 0.0001f, "incorrect prediction value");
	zassert_true(output > 0.146846071f - 0.0001f, "incorrect prediction value");

	const float inputs_2[] = {1.0f, 0.0f, 1.0f};
	nn_predict(3, inputs_2, &output);
//	printf("value of output: %2.9f\n", output);
	zassert_true(output < 0.637458861 + 0.0001f, "incorrect sigmoid fun output");
	zassert_true(output > 0.637458861 - 0.0001f, "incorrect sigmoid fun output");
}

static void test_predict_two_hidden_layers_1_1()
{
	/*
	 * x1	|>    |> y
	 */
	float output = 15.0f;

	float weights_l0[3] =  {-7.34381f, 0.16169f, 2.7074f};
	float offset_l0 = 0.52781f;

	float weights_l1 = -5.06003f;
	float offset_l1 = 2.37772f;

	zassert_equal(nn_init(3), 0, "incorrect status code for first execution of init call");
	ctx.layer_count = 0;

	zassert_equal(nn_add_layer(1), 0, "correct call should not fail");
	zassert_equal(nn_set_neuron(0, 0, offset_l0, 3, weights_l0), 0, "correct call, should return 0");

	zassert_equal(nn_add_layer(1), 0, "correct call should not fail");
	zassert_equal(nn_set_neuron(1, 0, offset_l1, 1, &weights_l1), 0, "correct call, should return 0");

	zassert_equal(ctx.layers[1].neurons[0].offset, offset_l1, "offset is not set correctly");
	zassert_equal(ctx.layers[0].weight_count, 3, "layer configuration is not correctly set");

	const float inputs[] = {0.0f, 0.0f, 1.0f};
	nn_predict(3, inputs, &output);
	zassert_true(output < 0.156049f + 0.01f, "incorrect prediction value");
	zassert_true(output > 0.156049f - 0.01f, "incorrect prediction value");

//	printf("dot product of layer0 with offset : %2.9f\n", ctx.layers[0].neurons[0].dot_product);
	zassert_true(ctx.layers[0].neurons[0].dot_product < (3.23521f + 0.0001f), "incorrect dotproduct with offset value");
	zassert_true(ctx.layers[0].neurons[0].dot_product > (3.23521f - 0.0001f), "incorrect dotproduct with offset value");

//	printf("output of sigmoid of layer0 : %2.9f\n", ctx.layers[0].neurons[0].output);
	zassert_true(0.881942093 + 0.0001f > nn_calc_sigmoid(3.23521f, false), "incorrect sigmoid value");
	zassert_true(0.881942093 - 0.0001f < nn_calc_sigmoid(3.23521f, false), "incorrect sigmoid value");

//	printf("dot product of layer1 with offset : %2.9f\n", ctx.layers[1].neurons[0].dot_product);
	zassert_true(ctx.layers[1].neurons[0].dot_product < (-2.084933519f + 0.0001f), "incorrect dotproduct with offset value");
	zassert_true(ctx.layers[1].neurons[0].dot_product > (-2.084933519f - 0.0001f), "incorrect dotproduct with offset value");

//	printf("output of sigmoid of layer1 : %2.9f\n", ctx.layers[1].neurons[0].output);
	zassert_true(0.162078053f + 0.0001f > nn_calc_sigmoid(-2.084933519f, false), "incorrect sigmoid value");
	zassert_true(0.162078053f - 0.0001f < nn_calc_sigmoid(-2.084933519f, false), "incorrect sigmoid value");

	const float inputs_2[] = {1.0f, 0.0f, 1.0f};
	nn_predict(3, inputs_2, &output);
//	printf("value of output: %2.0f\n", output);
	zassert_true(output < 0.8184119f + 0.01f, "incorrect sigmoid fun output");
	zassert_true(output > 0.8184119f - 0.01f, "incorrect sigmoid fun output");
}

static void test_add_layers()
{
	zassert_equal(nn_add_layer(0), -EIO, "adding incorrect number of neurons should fail");
	zassert_equal(nn_add_layer(MAX_NEURON_COUNT), -EIO, "adding incorrect number of neurons should fail");
	zassert_equal(nn_add_layer(MAX_NEURON_COUNT + 1), -EIO, "adding incorrect number of neurons should fail");
	zassert_equal(nn_add_layer(-1), -EIO, "adding incorrect number of neurons should fail");

	ctx.layer_count = MAX_LAYER_COUNT;
	zassert_equal(nn_add_layer(MAX_NEURON_COUNT - 1), -EIO, "too many layers configured, should fail");
	ctx.layer_count = MAX_LAYER_COUNT + 1;
	zassert_equal(nn_add_layer(MAX_NEURON_COUNT - 1), -EIO, "too many layers configured, should fail");


	zassert_equal(nn_init(7), 0, "incorrect status code for first execution of init call");
	ctx.layer_count = 0;
	zassert_equal(nn_add_layer(5), 0, "correct call should not fail");
	zassert_equal(ctx.layer_count, 1, "layer configuration is not correctly set");
	zassert_equal(ctx.layers[0].neuron_count, 5, "layer configuration is not correctly set");
	zassert_equal(ctx.layers[0].weight_count, 7, "layer configuration is not correctly set");

	zassert_equal(nn_add_layer(15), 0, "correct call should not fail");
	zassert_equal(ctx.layer_count, 2, "layer configuration is not correctly set");
	zassert_equal(ctx.layers[1].neuron_count, 15, "layer configuration is not correctly set");
	zassert_equal(ctx.layers[1].weight_count, 5, "layer configuration is not correctly set");
}

void test_set_neuron()
{
	int inputs = 7;
	int neuron_count = 5;
	zassert_equal(nn_init(inputs), 0, "incorrect status code for first execution of init call");
	zassert_equal(nn_add_layer(neuron_count), 0, "correct call should not fail");
	zassert_equal(ctx.layer_count, 1, "layer configuration is not correctly set");
	float weights[MAX_INPUT_COUNT] = {};

	zassert_equal(nn_set_neuron(ctx.layer_count, 2, 0, 3, weights), -EIO, "incorrect layer id, function should fail with -EIO");
	zassert_equal(nn_set_neuron(ctx.layer_count + 1, 2, 0, 3, weights), -EIO, "incorrect layer id, function should fail with -EIO");
	zassert_equal(nn_set_neuron(-1, 2, 0, 3, weights), -EIO, "incorrect layer id, function should fail with -EIO");


	zassert_equal(nn_set_neuron(0, 2, 0, 3, weights), -EIO, "incorrect number of weights, should return -EIO");
	zassert_equal(nn_set_neuron(0, 2, 0, 8, weights), -EIO, "incorrect number of weights, should return -EIO");
	zassert_equal(nn_set_neuron(0, 2, 0, 12, weights), -EIO, "incorrect number of weights, should return -EIO");

	zassert_equal(nn_set_neuron(0, MAX_NEURON_COUNT, 0, inputs, weights), -EIO, "incorrect neuron id, should return -EIO");
	zassert_equal(nn_set_neuron(0, MAX_NEURON_COUNT + 1, 0, inputs, weights), -EIO, "incorrect neuron id, should return -EIO");
	zassert_equal(nn_set_neuron(0, -1, 0, inputs, weights), -EIO, "incorrect neuron id, should return -EIO");

	weights[0] = 12.0f;
	weights[1] = 13.1f;
	weights[2] = 54343.3f;
	weights[3] = 7343.3f;
	weights[4] = 56343.3f;
	weights[5] = 33343.3f;
	weights[6] = 32343.3f;

	zassert_equal(nn_set_neuron(0, 1, 23.4f, inputs, weights), 0, "correct call, should return 0");

	int weight_id;
	for(weight_id = 0; weight_id < inputs ; weight_id++){
		zassert_equal(ctx.layers[0].neurons[1].weights[weight_id], weights[weight_id], "weights are not correctly set for neuron");
	}
	zassert_equal(ctx.layers[0].neurons[1].offset, 23.4f, "offset is not set correctly");
}

void test_get_layer_output()
{
	struct neural_network_layer layer = {
			.neuron_count = 3,
			.neurons = {
					{.output = 0.12f},
					{.output = 0.14f},
					{.output = -25.43f}
			}
	};

	float *outputs = nn_get_layer_output(&layer);
	int i = 0;
	for (i = 0; i < 3; i++){
		zassert_equal(layer.neurons[i].output, outputs[i], "output is not correctly copied over");
	}

	struct neural_network_layer layer_2 = {
			.neuron_count = MAX_NEURON_COUNT,
			.neurons = {
					{.output = 0.12f},
					{.output = 0.14f},
					{.output = -345.43f},
					{.output = 5645.43f},
					{.output = 43.0f},
					{.output = 543.43f},
					{.output = 54354.43f},
					{.output = 122.43f},
					{.output = 433.43f},
					{.output = 77.43f},
					{.output = 423.43f},
					{.output = -23.43f},
					{.output = 673.43f},
					{.output = 533.43f},
					{.output = 433.43f},
					{.output = 13.43f}
			}
	};
	outputs = nn_get_layer_output(&layer_2);
	for (i = 0; i < MAX_NEURON_COUNT; i++){
		zassert_equal(layer_2.neurons[i].output, outputs[i], "output is not correctly copied over");
	}

}

void test_setup()
{
	memset(&ctx, 0, sizeof(struct neural_network_context));
}

void test_teardown()
{
	;;
}

void test_main(void)
{
	ztest_test_suite(nn_tests,
		ztest_unit_test(init_tests),
		ztest_user_unit_test_setup_teardown(test_add_layers, test_setup, test_teardown),
		ztest_user_unit_test_setup_teardown(test_set_neuron, test_setup, test_teardown),
		ztest_unit_test(test_sigmoid),
		ztest_unit_test(test_dot_product),
		ztest_unit_test(test_get_layer_output),
		ztest_user_unit_test_setup_teardown(test_predict, test_setup, test_teardown),
		ztest_user_unit_test_setup_teardown(test_predict_two_hidden_layers_3_1, test_setup, test_teardown),
		ztest_user_unit_test_setup_teardown(test_predict_two_hidden_layers_1_1, test_setup, test_teardown)
	);

	ztest_run_test_suite(nn_tests);
}
