/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <shell/shell.h>
#include <string.h>

#include "../../neural_net_101/build/zephyr/include/generated/version.h"
#include "../../neural_net_101/src/neural_network.h"
#include "../../neural_net_101/src/utils.h"

#define DBG_PRINTF_PREFIX 			"shell> "
#define DBG_PRINTF(fmt, ...)		printk(DBG_PRINTF_PREFIX fmt "\n", ##__VA_ARGS__)

#define SENSOR_DATA_MIN_COUNT 5

static int shell_cmd_ping(int argc, char *argv[])
{
	ARG_UNUSED(argc);
	ARG_UNUSED(argv);

	printk("pong\n");

	return 0;
}

static int shell_cmd_params(int argc, char *argv[])
{
	int cnt;

	printk("argc = %d\n", argc);
	for (cnt = 0; cnt < argc; cnt++) {
		printk("  argv[%d] = %s\n", cnt, argv[cnt]);
	}
	return 0;
}


static int shell_cmd_send_parameter(int argc, char *argv[])
{
	ARG_UNUSED(argc);
	ARG_UNUSED(argv);

	if(argc < SENSOR_DATA_MIN_COUNT){
		printk("too few input arguments\n");
	}

	printk("received telemetry data");

	return 0;
}

static int shell_cmd_version(int argc, char *argv[])
{
	ARG_UNUSED(argc);
	ARG_UNUSED(argv);

	printk("Zephyr version %s\n", KERNEL_VERSION_STRING);
	return 0;
}

#define PREDICT_ARG_COUNT 3
static int shell_cmd_predict(int argc, char *argv[])
{
	DBG_PRINTF("predict request");
	if (argc != PREDICT_ARG_COUNT + 1){
		DBG_PRINTF("received incorrect number of args, provide binary values for prediction, eg. predict 0 0 1");
		return 0;
	}

	float inputs[PREDICT_ARG_COUNT] = {};

	printk("<- input vector: ");
	int cnt;
	for (cnt = 1; cnt < argc; cnt++) {
		printk("%s ", argv[cnt]);
		inputs[cnt - 1] = stof(argv[cnt]);
	}
	printk("\n");

	float output = 0.0f;
	if(nn_predict(PREDICT_ARG_COUNT, (const float*)inputs, &output) == 0){
		char output_s[30] = {};
		dtoa_grisu3(output, output_s);
		printk("-> predicted value: %s\n", output_s);
	}

	return 0;
}

static int shell_cmd_get_weights(int argc, char *argv[])
{
	printk("weights request\n");
	return 0;
}

static int shell_cmd_get_struct(int argc, char *argv[])
{
	printk("struct request\n");
	return 0;
}

static int shell_cmd_get_hist(int argc, char *argv[])
{
	printk("hist request\n");

	return 0;
}

#define NEURAL_SHELL_MODULE "neural"

static struct shell_cmd commands[] = {
	{ "ping", shell_cmd_ping, NULL },
	{ "version", shell_cmd_version, "zephyr rtos version" },
	{ "predict", shell_cmd_predict, NULL },
	{ "weights", shell_cmd_get_weights, NULL },
	{ "struct", shell_cmd_get_struct, NULL },
	{ "hist", shell_cmd_get_hist, NULL },
	{ NULL, NULL, NULL }
};

void setup_nn()
{
	int input_count = 3;
	int neuron_count_l0 = 1;
	float weights[3] = {34.62191f, -2.279318f, -14.5473f};

	printk("### initializing inputs, count: %d\n", input_count);
	nn_init(input_count);

	printk("### adding layer with %d neurons \n", neuron_count_l0);
	nn_add_layer(neuron_count_l0);

	printk("### setting up neuron weights: \n");

	char output[30] = {};
	int weight_id = 0;
	for (weight_id = 0; weight_id < input_count; weight_id++){
		dtoa_grisu3(weights[weight_id], output);
		printk("   %s\n", output);
	}

	nn_set_neuron(0, 0, 0, input_count, weights);

	printk("######### finished setting up neural network\n");
}

void main(void)
{

	printk("\n\n######### Sample Neural Network demo booting, arch: %s\n", CONFIG_ARCH);
	setup_nn();

	SHELL_REGISTER(NEURAL_SHELL_MODULE, commands);
	shell_register_default_module(NEURAL_SHELL_MODULE);
}
