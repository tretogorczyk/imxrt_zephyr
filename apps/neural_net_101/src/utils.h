/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _UTILS_H_
#define _UTILS_H_
#include <zephyr.h>
#include <misc/printk.h>
#include <shell/shell.h>


/// Converts the given double-precision floating point number to a string representation.

/** For most inputs, this string representation is the

	shortest such, which deserialized again, returns the same bit

	representation of the double.

	@param v The number to convert.

	@param dst [out] The double-precision floating point number will be written here

		as a null-terminated string. The conversion algorithm will write at most 25 bytes

		to this buffer. (null terminator is included in this count).

		The dst pointer may not be null.

	@return the number of characters written to dst, excluding the null terminator (which

		is always written) is returned here. */

int dtoa_grisu3(double v, char *dst);

/*
 * @brief: parses input string to float
 */
float stof(const char* s);

#endif /* _UTILS_H_*/
