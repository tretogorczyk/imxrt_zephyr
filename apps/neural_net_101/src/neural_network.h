/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _NEURAL_NETWORK_H_
#define _NEURAL_NETWORK_H_
#include <zephyr.h>
#include <misc/printk.h>
#include <shell/shell.h>

// TODO: These should naturally not be hard-coded like this, instead - allocate through mem pool for speed
#define MAX_INPUT_COUNT 16
#define MAX_NEURON_COUNT 16
#define MAX_LAYER_COUNT 4

/*
 * @brief: Adds a new hidden layer of neurons to the neural network
 *
 * @param neuron_count - number of neurons to add in this hidden layer
 *
 * @retval: 0 on success, other completion codes on failure
 */
int nn_add_layer(int neuron_count);

/*
 * @brief: Sets specific neuron's weights and offset
 *
 * @param layer_id - id of the hidden layer
 * @param neuron_id - id of the neuron to set
 * @param offset - fixed offset value added to the dotproduct of this neuron
 * @param weight_cnt - number of weights, this MUST match the number of neurons in previous layer or number of inputs
 * @param weights - vector of weights for each input
 */
int nn_set_neuron(int layer_id, int neuron_id, float offset, int weight_cnt, float weights[MAX_INPUT_COUNT]);

/*
 * @brief: Uses neural network to predict output
 *
 * @param input_count - number of elements in inputs vector
 * @param inputs - pointer to table of float inputs
 * @param output - output variable with prediction value
 *
 * @retval 0 on success, other completion codes on failure
 */
int nn_predict(int input_count, const float *inputs, float *output);

/*
 * @brief: Prints the neural network topology
 */
//TODO:
//int nn_print_topology();

/*
 * @brief: initializes the context of the neural network
 *
 * @param number_of_inputs - number of input values, the output is always just one currently
 *
 * @retval: 0 on success, other completion codes on failure
 */
int nn_init(int number_of_inputs);

#endif /* _NEURAL_NETWORK_H_*/
