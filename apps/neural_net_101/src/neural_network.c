/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "../../neural_net_101/src/neural_network.h"

#include <zephyr.h>
#include <misc/printk.h>

#define DBG_PRINTF_PREFIX 			"nn> "
#define DBG_PRINTF(fmt, ...)		printk(DBG_PRINTF_PREFIX fmt "\n", ##__VA_ARGS__)
struct neural_network_neuron {
	float weights[MAX_INPUT_COUNT];
	float offset;
	float dot_product;
	float output;
};

struct neural_network_layer {
	int neuron_count;
	int weight_count;
	bool is_full_conv;
	struct neural_network_neuron neurons[MAX_NEURON_COUNT];
	int (*activation_func)(int, bool);
};

struct neural_network_context {
	bool is_initialized;
	int number_of_inputs;
	int layer_count;
	int neuron_count;
	struct neural_network_layer layers[MAX_LAYER_COUNT];
};
static struct neural_network_context ctx = {};

static float layer_output_buffer[MAX_NEURON_COUNT] = {};

#define ABS(x) (x > 0 ? (x) : (x*-1))

/*
 * @brief: Calculates an approximated sigmoid function
 */
static float nn_calc_sigmoid(float x, bool is_derivative)
{
	double input = (double) x;
	if(is_derivative == true){
		return (float) input*(1.0f - input);
	} else {
		return (float)(0.5f * (input / (1.0f + ABS(input))) + 0.5f);
	}
}

/*
 * @brief: Calculates a dot product of two vectors
 *
 * @param size 	- number of elements in a and b, vector sizes must match
 * @param a 	- vector a
 * @param b 	- vector b
 *
 * @retval - dot product of vectors a and b
 */
static float nn_calc_dot_product(int size, const float *a, const float *b)
{
	double output = 0;

	int elem_id;
	for (elem_id = 0 ; elem_id < size ; elem_id++){
		output += a[elem_id] * b[elem_id];
	}
	return (float)output;
}

/*
 * @brief: Returns in an array the output of all neurons of given layer
 *
 * @param layer - handle to layer
 *
 * @retval handle to array with layer outputs
 */
static float* nn_get_layer_output(struct neural_network_layer *layer)
{
	int neuron_count = layer->neuron_count;
	int neuron_id;
	for (neuron_id = 0 ; neuron_id < neuron_count ; neuron_id++){
		layer_output_buffer[neuron_id] = layer->neurons[neuron_id].output;
	}
	return layer_output_buffer;
}

/*
 * @brief: Uses neural network to predict output
 *
 * @param input_count - number of elements in inputs vector
 * @param inputs - pointer to table of float inputs
 * @param output - output variable with prediction value
 *
 * @retval 0 on success, other completion codes on failure
 */
int nn_predict(int input_count, const float *inputs, float *output)
{
	if(ctx.number_of_inputs != input_count){
		DBG_PRINTF("incorrect number of input values, expected %d, received %d", ctx.number_of_inputs, input_count);
		return -EIO;
	}
	if(inputs == NULL || output == NULL){
		DBG_PRINTF("null ptr provided");
		return -ENOENT;
	}

	if(ctx.layer_count == 0){
		DBG_PRINTF("no layers configured");
		return -EIO;
	}

	int layer_id;
	const float *layer_input = inputs;
	struct neural_network_layer *layer = &ctx.layers[0];
	struct neural_network_neuron *neuron = NULL;

	for(layer_id = 0; layer_id < ctx.layer_count ; layer_id++){
		if(layer_id != 0){
			layer_input = nn_get_layer_output(layer);
		}
		layer = &ctx.layers[layer_id];

		int neuron_id;
		for (neuron_id = 0 ; neuron_id < layer->neuron_count ; neuron_id++){
			neuron = &layer->neurons[neuron_id];
			neuron->dot_product = nn_calc_dot_product(layer->weight_count, layer_input, (const float*)neuron->weights);
			neuron->dot_product += neuron->offset;
			neuron->output = nn_calc_sigmoid(neuron->dot_product, false);
		}
	}

	*output = neuron->output;

	return 0;
}

/*
 * @brief: Adds a new hidden layer of neurons to the neural network
 *
 * @param neuron_count - number of neurons to add in this hidden layer
 *
 * @retval: 0 on success, other completion codes on failure
 */
int nn_add_layer(int neuron_count)
{
	if(neuron_count <= 0 || neuron_count >= MAX_NEURON_COUNT){
		DBG_PRINTF("illegal num of neurons in a layer");
		return -EIO;
	}
	int layer_id = ctx.layer_count;
	if(layer_id >= MAX_LAYER_COUNT){
		DBG_PRINTF("too many layers configured");
		return -EIO;
	}

	struct neural_network_layer *layer = &ctx.layers[layer_id];
	layer->neuron_count = neuron_count;

	if(layer_id == 0){
		layer->weight_count = ctx.number_of_inputs;
	} else {
		layer->weight_count = ctx.layers[layer_id - 1].neuron_count;
	}

	ctx.layer_count++;

	return 0;
}

/*
 * @brief: Sets specific neuron's weights and offset
 *
 * @param layer_id - id of the hidden layer
 * @param neuron_id - id of the neuron to set
 * @param offset - fixed offset value added to the dotproduct of this neuron
 * @param weight_cnt - number of weights, this MUST match the number of neurons in previous layer or number of inputs
 * @param weights - vector of weights for each input
 */
int nn_set_neuron(int layer_id, int neuron_id, float offset, int weight_cnt, float weights[MAX_INPUT_COUNT])
{
	if(layer_id < 0 || layer_id >= ctx.layer_count){
		DBG_PRINTF("incorrect layer_id");
		return -EIO;
	}

	struct neural_network_layer *layer = &ctx.layers[layer_id];
	if(layer->weight_count != weight_cnt || weight_cnt > MAX_INPUT_COUNT){
		DBG_PRINTF("incorrect number of weights provided");
		return -EIO;
	}

	if(neuron_id >= layer->neuron_count || neuron_id < 0){
		DBG_PRINTF("incorrect neuron id");
		return -EIO;
	}

	struct neural_network_neuron *neuron = &layer->neurons[neuron_id];
	neuron->offset = offset;
	memcpy(neuron->weights, weights, sizeof(float)*weight_cnt);

	return 0;
}

/*
 * @brief: initializes the context of the neural network
 *
 * @param number_of_inputs - number of input values, the output is always just one currently
 *
 * @retval: 0 on success, other completion codes on failure
 */
int nn_init(int number_of_inputs)
{
	if(ctx.is_initialized == true){
		DBG_PRINTF("already initialized");
		return -EIO;
	}

	if(number_of_inputs >= MAX_INPUT_COUNT || number_of_inputs <= 0){
		DBG_PRINTF("incorrect number of inputs to neural network");
		return -EIO;
	}

	ctx.number_of_inputs = number_of_inputs;
	ctx.is_initialized = true;
	DBG_PRINTF("init done");

	return 0;
}
