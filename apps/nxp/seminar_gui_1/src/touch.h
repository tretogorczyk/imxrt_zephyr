/*
 * touch.h
 *
 *  Created on: Apr 4, 2018
 *      Author: A89217
 */

#ifndef _ZEPHYR_APPS_NXP_SAMPLE_GUI_SRC_TOUCH_H_
#define _ZEPHYR_APPS_NXP_SAMPLE_GUI_SRC_TOUCH_H_

/*
 * @brief: Initializes touch panel, transport, etc.
 */
int touch_init(void);

#endif /* _ZEPHYR_APPS_NXP_SAMPLE_GUI_SRC_TOUCH_H_ */
