/*
 * Copyright (c) 2015 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <version.h>


#include "fsl_common.h"
#include "fsl_elcdif.h"
//#include "fsl_debug_console.h"
#include "fsl_lpi2c.h"
#include "fsl_igpio.h"
#include "clock_config.h"

#include "fsl_lpi2c.h"

#include "touch.h"

#define DBG_PRINTF_PREFIX 			"touch> "
#define DBG_PRINTF(fmt, ...)		printk(DBG_PRINTF_PREFIX fmt "\n", ##__VA_ARGS__)

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define EXAMPLE_LPI2C_MASTER_BASEADDR LPI2C1
#define EXAMPLE_LPI2C_SLAVE_BASEADDR LPI2C3

/* Select USB1 PLL (480 MHz) as master lpi2c clock source */
#define LPI2C_CLOCK_SOURCE_SELECT (0U)
/* Clock divider for master lpi2c clock source */
#define LPI2C_CLOCK_SOURCE_DIVIDER (5U)
/* Get frequency of lpi2c clock */
#define LPI2C_CLOCK_FREQUENCY ((CLOCK_GetFreq(kCLOCK_Usb1PllClk) / 8) / (LPI2C_CLOCK_SOURCE_DIVIDER + 1U))

#define LPI2C_MASTER_CLOCK_FREQUENCY LPI2C_CLOCK_FREQUENCY
#define LPI2C_SLAVE_CLOCK_FREQUENCY LPI2C_CLOCK_FREQUENCY

#define LPI2C_MASTER_IRQ LPI2C1_IRQn
#define LPI2C_SLAVE_IRQ LPI2C3_IRQn

#define LPI2C_MASTER_IRQHandler LPI2C1_IRQHandler
#define LPI2C_SLAVE_IRQHandler LPI2C3_IRQHandler
#define LPI2C_MASTER_SLAVE_ADDR_7BIT 0x7EU
#define LPI2C_BAUDRATE 100000U
#define LPI2C_DATA_LENGTH 32U

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/

uint8_t g_slave_buff[LPI2C_DATA_LENGTH] = {0};
uint8_t g_master_buff[LPI2C_DATA_LENGTH] = {0};
volatile uint8_t g_masterTxIndex = 0U;
volatile uint8_t g_masterRxIndex = 0U;
volatile uint8_t g_slaveTxIndex = 0U;
volatile uint8_t g_slaveRxIndex = 0U;
volatile bool g_masterReadBegin = false;

void exampe_isr_handler(void *arg)
{
	DBG_PRINTF("im here");
//   ... /* ISR code */
}

int touch_init(void)
{
	lpi2c_slave_config_t slaveConfig = {0};
	lpi2c_master_config_t masterConfig = {0};
	status_t reVal = kStatus_Fail;

    /*Clock setting for LPI2C*/
    CLOCK_SetMux(kCLOCK_Lpi2cMux, LPI2C_CLOCK_SOURCE_SELECT);
    CLOCK_SetDiv(kCLOCK_Lpi2cDiv, LPI2C_CLOCK_SOURCE_DIVIDER);

//    IRQ_CONNECT(MY_DEV_IRQ, 2, exampe_isr_handler, MY_ISR_ARG, 0);
//    irq_enable(MY_DEV_IRQ);            /* enable IRQ */

    /* 1.Set up lpi2c slave first */
    /*
     * slaveConfig.address0 = 0U;
     * slaveConfig.address1 = 0U;
     * slaveConfig.addressMatchMode = kLPI2C_MatchAddress0;
     * slaveConfig.filterDozeEnable = true;
     * slaveConfig.filterEnable = true;
     * slaveConfig.enableGeneralCall = false;
     * slaveConfig.ignoreAck = false;
     * slaveConfig.enableReceivedAddressRead = false;
     * slaveConfig.sdaGlitchFilterWidth_ns = 0;
     * slaveConfig.sclGlitchFilterWidth_ns = 0;
     * slaveConfig.dataValidDelay_ns = 0;
     * slaveConfig.clockHoldTime_ns = 0;
     */
    LPI2C_SlaveGetDefaultConfig(&slaveConfig);
    slaveConfig.address0 = LPI2C_MASTER_SLAVE_ADDR_7BIT;

    LPI2C_SlaveInit(EXAMPLE_LPI2C_SLAVE_BASEADDR, &slaveConfig, LPI2C_SLAVE_CLOCK_FREQUENCY);

    LPI2C_MasterInit(EXAMPLE_LPI2C_MASTER_BASEADDR, &masterConfig, LPI2C_MASTER_CLOCK_FREQUENCY);
	return 0;
}
