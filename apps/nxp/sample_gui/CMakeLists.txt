
include($ENV{ZEPHYR_BASE}/cmake/app/boilerplate.cmake NO_POLICY_SCOPE)
project(NONE)

target_sources(app PRIVATE src/main.c)
target_sources(app PRIVATE src/imagelib.c)
target_sources(app PRIVATE src/neural_net/neural_net.c)
#target_sources(app PRIVATE src/touch.c) temporarily disabled
target_sources(app PRIVATE src/video/fsl_csi_camera_adapter.c)
target_sources(app PRIVATE src/video/fsl_ov7725.c)
target_sources(app PRIVATE src/video/fsl_sccb.c)