/*
 * neural_net.h
 *
 *  Created on: Apr 4, 2018
 *      Author: A89217
 */

#ifndef _ZEPHYR_APPS_NXP_SAMPLE_GUI_NEURAL_NET_H_
#define _ZEPHYR_APPS_NXP_SAMPLE_GUI_NEURAL_NET_H_

#include "nn/arm_nn_tables.h"
#include "nn/arm_nnfunctions.h"
#include "nn/arm_nnsupportfunctions.h"

/*
 * @brief: Initializes neural net
 */
int neural_net_init(void);

int neural_net_predict_image_example(void);

int neural_net_infer(uint16_t *bufer);

#endif /* _ZEPHYR_APPS_NXP_SAMPLE_GUI_NEURAL_NET_H_ */
