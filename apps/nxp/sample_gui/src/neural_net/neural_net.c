/*
 * Copyright (c) 2015 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <version.h>

#include "neural_net.h"

#include "arm_nnexamples_cifar10_parameter.h"
#include "arm_nnexamples_cifar10_shift.h"
#include "arm_nnexamples_cifar10_weights.h"
#include "arm_nnexamples_cifar10_inputs.h"
#include "arm_nnexamples_cifar10_meanarr.h"


#define DBG_PRINTF_PREFIX 			"nn> "
#define DBG_PRINTF(fmt, ...)		printk(DBG_PRINTF_PREFIX fmt "\n", ##__VA_ARGS__)

#define FULLCON_WT {35, -2, 15}
#define FULLCON_BIAS {-5}


static uint32_t sample_dog[32 * 32 * 3] = IMG_DATA;

static q7_t conv1_wt[CONV1_IM_CH * CONV1_KER_DIM * CONV1_KER_DIM * CONV1_OUT_CH] = CONV1_WT;
static q7_t conv1_bias[CONV1_OUT_CH] = CONV1_BIAS;

static q7_t conv2_wt[CONV2_IM_CH * CONV2_KER_DIM * CONV2_KER_DIM * CONV2_OUT_CH] = CONV2_WT;
static q7_t conv2_bias[CONV2_OUT_CH] = CONV2_BIAS;

static q7_t conv3_wt[CONV3_IM_CH * CONV3_KER_DIM * CONV3_KER_DIM * CONV3_OUT_CH] = CONV3_WT;
static q7_t conv3_bias[CONV3_OUT_CH] = CONV3_BIAS;

static q7_t ip1_wt[IP1_DIM * IP1_OUT] = IP1_WT;
static q7_t ip1_bias[IP1_OUT] = IP1_BIAS;

//q7_t      input_data[CONV1_IM_CH * CONV1_IM_DIM * CONV1_IM_DIM] = IMG_DATA;
q7_t	  input_data[CONV1_IM_CH * CONV1_IM_DIM * CONV1_IM_DIM] = IMG_DATA; //CAT //DOG
q7_t      output_data[IP1_OUT];

/*	Mean array that is subtraccted from every image pixel in HWC format */
q7_t 	mean_arr[CONV1_IM_CH * CONV1_IM_DIM * CONV1_IM_DIM] = MEAN_ARR_REORD;

//vector buffer: max(im2col buffer,average pool buffer, fully connected buffer)
q7_t      col_buffer[2 * 5 * 5 * 32 * 2];

q7_t      scratch_buffer[32 * 32 * 10 * 4];

//TODO: seems that this is not supported, why?
//#define ARM_MATH_DSP

static q7_t fullcon_wt[3] = FULLCON_WT;
static q7_t fullcon_bias[1] = FULLCON_BIAS;

const static q7_t inputs[4][3] = {
		{0, 0, 1},
		{1, 0, 1},
		{0, 1, 1},
		{1, 1, 1}
};

/*
 * @brief Simple neural net prediction
 * @param[in]		input_vector	pointer to input
 * @return none
 */
int neural_net_predict(const q7_t *input_vector)
{
	DBG_PRINTF("start prediction (simple), input vector: [%d, %d, %d]", input_vector[0], input_vector[1], input_vector[2]);
	  /* start the execution */
	 q7_t output = 0;
	 arm_fully_connected_q7(input_vector, fullcon_wt, 3, 1, 0, 0, fullcon_bias, &output, NULL);
	 arm_nn_activations_direct_q7(&output, 1, 2, ARM_TANH);
	 DBG_PRINTF("output: %d", output);
	 return 0;
}


#define BITMASK_15_TO_11	0xF800
#define BITMASK_10_TO_5 	0x7E0
#define BITMASK_4_TO_0  	0x1F
#define PIXEL_COUNT (32 * 32 * 3)

q7_t outp_buffer[32 * 32 * 3];
static void neural_net_hwc_to_neural(uint8_t *buffer)
{
	DBG_PRINTF(">> CONVERTING HWC TO NEURAL");
//#define DBG_PRINTF_FOR_HWC_TO_NEURAL
	int i;

#ifdef DBG_PRINTF_FOR_HWC_TO_NEURAL
	DBG_PRINTF("input values:");
	for (i = 0 ; i < 10 ; i++){
		printk("%d,", buffer[i]);
	}
	printk("\n");
#endif

	for (i = 0; i < PIXEL_COUNT ; i++){
		int value = (int)buffer[i];
		value -= (int)mean_arr[i];
//		if(value > 127){
//			value = 127;
//		}
//		if(value < -128){
//			value = -128;
//		}
		outp_buffer[i] = (q7_t)value;
	}

#ifdef DBG_PRINTF_FOR_HWC_TO_NEURAL
	DBG_PRINTF("output values:");
	for (i = 0 ; i < 10 ; i++){
		printk("%d,", outp_buffer[i]);
	}
	printk("\n");
#endif
}

/*
 * @brief: Parses input buffer in RGB565 format to accepted by neural network - RGBRGB... on byte each
 *
 * @param[in] buffer		Handle to input buffer with image
 */
static void neural_net_rgb565_to_hwc(uint16_t *buffer)
{
	DBG_PRINTF(">> CONVERTING RGB565 to HWC");
//#define DBG_PRINTF_RGB565_TO_HWC
#ifdef DBG_PRINTF_RGB565_TO_HWC
	int i = 0;
	DBG_PRINTF("input values:");
	for (i = 0 ; i < 10 ; i++){
		printk("%d,", buffer[i]);
	}
	printk("\n");
#endif

	/* FIX THIS, must get an image as from camera, parse it to their format and correctly identify!*/
	int input;
	int pixel_id;
	int pixel_count = 32*32;
	for (pixel_id = 0 ; pixel_id < PIXEL_COUNT; pixel_id++){
		input = buffer[pixel_id];
		q7_t rl = ((input & BITMASK_15_TO_11) >> 11);
		q7_t gl = ((input & BITMASK_10_TO_5) >> 5);
		q7_t bl = ((input & BITMASK_4_TO_0));

//		q7_t rl = (((input & BITMASK_15_TO_11) >> 11) << 3);
//		q7_t gl = (((input & BITMASK_10_TO_5) >> 5) << 2);
//		q7_t bl = (((input & BITMASK_4_TO_0)) << 3);
		input_data[(pixel_id*3)] = rl;
		input_data[(pixel_id*3)+1] = gl;
		input_data[(pixel_id*3)+2] = bl;
	}

#ifdef DBG_PRINTF_RGB565_TO_HWC
	DBG_PRINTF("output values:");
	for (i = 0 ; i < 10 ; i++){
		printk("%d,", input_data[i]);
	}
	printk("\n");
#endif
}


uint8_t raw_png[32 * 32 * 3] = TEST_PNG_RAW;

int neural_net_infer(uint16_t *bufer)
{
	DBG_PRINTF(">> START EXECUTION - IMAGE INFERENCE");
//	neural_net_convert_image_to_cifar((uint16_t*)sample_dog);
	neural_net_rgb565_to_hwc(bufer);
	neural_net_hwc_to_neural(raw_png);

	DBG_PRINTF(">> CLASSIFYING...");
	  q7_t     *img_buffer1 = scratch_buffer;
	  q7_t     *img_buffer2 = img_buffer1 + 32 * 32 * 32;


	  // conv1 input_data -> img_buffer1
	  arm_convolve_HWC_q7_RGB(outp_buffer, CONV1_IM_DIM, CONV1_IM_CH, conv1_wt, CONV1_OUT_CH, CONV1_KER_DIM, CONV1_PADDING,
							  CONV1_STRIDE, conv1_bias, CONV1_BIAS_LSHIFT, CONV1_OUT_RSHIFT, img_buffer1, CONV1_OUT_DIM,
							  (q15_t *) col_buffer, NULL);

	  arm_relu_q7(img_buffer1, CONV1_OUT_DIM * CONV1_OUT_DIM * CONV1_OUT_CH);

	  // pool1 img_buffer1 -> img_buffer2
	  arm_maxpool_q7_HWC(img_buffer1, CONV1_OUT_DIM, CONV1_OUT_CH, POOL1_KER_DIM,
						 POOL1_PADDING, POOL1_STRIDE, POOL1_OUT_DIM, NULL, img_buffer2);

	  // conv2 img_buffer2 -> img_buffer1
	  arm_convolve_HWC_q7_fast(img_buffer2, CONV2_IM_DIM, CONV2_IM_CH, conv2_wt, CONV2_OUT_CH, CONV2_KER_DIM,
							   CONV2_PADDING, CONV2_STRIDE, conv2_bias, CONV2_BIAS_LSHIFT, CONV2_OUT_RSHIFT, img_buffer1,
							   CONV2_OUT_DIM, (q15_t *) col_buffer, NULL);

	  arm_relu_q7(img_buffer1, CONV2_OUT_DIM * CONV2_OUT_DIM * CONV2_OUT_CH);

	  // pool2 img_buffer1 -> img_buffer2
	  arm_avepool_q7_HWC(img_buffer1, CONV2_OUT_DIM, CONV2_OUT_CH, POOL2_KER_DIM,
						 POOL2_PADDING, POOL2_STRIDE, POOL2_OUT_DIM, col_buffer, img_buffer2);

	// conv3 img_buffer2 -> img_buffer1
	  arm_convolve_HWC_q7_fast(img_buffer2, CONV3_IM_DIM, CONV3_IM_CH, conv3_wt, CONV3_OUT_CH, CONV3_KER_DIM,
							   CONV3_PADDING, CONV3_STRIDE, conv3_bias, CONV3_BIAS_LSHIFT, CONV3_OUT_RSHIFT, img_buffer1,
							   CONV3_OUT_DIM, (q15_t *) col_buffer, NULL);

	  arm_relu_q7(img_buffer1, CONV3_OUT_DIM * CONV3_OUT_DIM * CONV3_OUT_CH);

	  // pool3 img_buffer-> img_buffer2
	  arm_avepool_q7_HWC(img_buffer1, CONV3_OUT_DIM, CONV3_OUT_CH, POOL3_KER_DIM,
						 POOL3_PADDING, POOL3_STRIDE, POOL3_OUT_DIM, col_buffer, img_buffer2);

	#ifdef IP_X4
	  arm_fully_connected_q7_opt(img_buffer2, ip1_wt, IP1_DIM, IP1_OUT, IP1_BIAS_LSHIFT, IP1_OUT_RSHIFT, ip1_bias,
								 output_data, (q15_t *) img_buffer1);
	#else
	  arm_fully_connected_q7(img_buffer2, ip1_wt, IP1_DIM, IP1_OUT, IP1_BIAS_LSHIFT, IP1_OUT_RSHIFT, ip1_bias,
							 output_data, (q15_t *) img_buffer1);
	#endif

	  arm_softmax_q7(output_data, 10, output_data);

	  DBG_PRINTF(">> CLASSIFICATION RESULT: (0 - not identified, 127 - identified)");
	  DBG_PRINTF("class 0 - airplane        %d", output_data[0]);
	  DBG_PRINTF("class 1 - automobile      %d", output_data[1]);
	  DBG_PRINTF("class 2 - bird            %d", output_data[2]);
	  DBG_PRINTF("class 3 - cat             %d", output_data[3]);
	  DBG_PRINTF("class 4 - deer            %d", output_data[4]);
	  DBG_PRINTF("class 5 - dog             %d", output_data[5]);
	  DBG_PRINTF("class 6 - frog            %d", output_data[6]);
	  DBG_PRINTF("class 7 - horse           %d", output_data[7]);
	  DBG_PRINTF("class 8 - ship            %d", output_data[8]);
	  DBG_PRINTF("class 9 - truck           %d", output_data[9]);
	  DBG_PRINTF("\n");
//	  for (int i = 0; i < 10; i++)
//	  {
//		  DBG_PRINTF("%d: %d", i, output_data[i]);
//	  }

	  return 0;
}

int neural_net_predict_image_example()
{
      DBG_PRINTF("start execution - pre-loaded image inference");
	  /* start the execution */

	  q7_t     *img_buffer1 = scratch_buffer;
	  q7_t     *img_buffer2 = img_buffer1 + 32 * 32 * 32;

	  // conv1 input_data -> img_buffer1
	  arm_convolve_HWC_q7_RGB(input_data, CONV1_IM_DIM, CONV1_IM_CH, conv1_wt, CONV1_OUT_CH, CONV1_KER_DIM, CONV1_PADDING,
	                          CONV1_STRIDE, conv1_bias, CONV1_BIAS_LSHIFT, CONV1_OUT_RSHIFT, img_buffer1, CONV1_OUT_DIM,
	                          (q15_t *) col_buffer, NULL);

	  arm_relu_q7(img_buffer1, CONV1_OUT_DIM * CONV1_OUT_DIM * CONV1_OUT_CH);

	  // pool1 img_buffer1 -> img_buffer2
	  arm_maxpool_q7_HWC(img_buffer1, CONV1_OUT_DIM, CONV1_OUT_CH, POOL1_KER_DIM,
	                     POOL1_PADDING, POOL1_STRIDE, POOL1_OUT_DIM, NULL, img_buffer2);

	  // conv2 img_buffer2 -> img_buffer1
	  arm_convolve_HWC_q7_fast(img_buffer2, CONV2_IM_DIM, CONV2_IM_CH, conv2_wt, CONV2_OUT_CH, CONV2_KER_DIM,
	                           CONV2_PADDING, CONV2_STRIDE, conv2_bias, CONV2_BIAS_LSHIFT, CONV2_OUT_RSHIFT, img_buffer1,
	                           CONV2_OUT_DIM, (q15_t *) col_buffer, NULL);

	  arm_relu_q7(img_buffer1, CONV2_OUT_DIM * CONV2_OUT_DIM * CONV2_OUT_CH);

	  // pool2 img_buffer1 -> img_buffer2
	  arm_avepool_q7_HWC(img_buffer1, CONV2_OUT_DIM, CONV2_OUT_CH, POOL2_KER_DIM,
	                     POOL2_PADDING, POOL2_STRIDE, POOL2_OUT_DIM, col_buffer, img_buffer2);

	// conv3 img_buffer2 -> img_buffer1
	  arm_convolve_HWC_q7_fast(img_buffer2, CONV3_IM_DIM, CONV3_IM_CH, conv3_wt, CONV3_OUT_CH, CONV3_KER_DIM,
	                           CONV3_PADDING, CONV3_STRIDE, conv3_bias, CONV3_BIAS_LSHIFT, CONV3_OUT_RSHIFT, img_buffer1,
	                           CONV3_OUT_DIM, (q15_t *) col_buffer, NULL);

	  arm_relu_q7(img_buffer1, CONV3_OUT_DIM * CONV3_OUT_DIM * CONV3_OUT_CH);

	  // pool3 img_buffer-> img_buffer2
	  arm_avepool_q7_HWC(img_buffer1, CONV3_OUT_DIM, CONV3_OUT_CH, POOL3_KER_DIM,
	                     POOL3_PADDING, POOL3_STRIDE, POOL3_OUT_DIM, col_buffer, img_buffer2);

	#ifdef IP_X4
	  arm_fully_connected_q7_opt(img_buffer2, ip1_wt, IP1_DIM, IP1_OUT, IP1_BIAS_LSHIFT, IP1_OUT_RSHIFT, ip1_bias,
	                             output_data, (q15_t *) img_buffer1);
	#else
	  arm_fully_connected_q7(img_buffer2, ip1_wt, IP1_DIM, IP1_OUT, IP1_BIAS_LSHIFT, IP1_OUT_RSHIFT, ip1_bias,
	                         output_data, (q15_t *) img_buffer1);
	#endif

	  arm_softmax_q7(output_data, 10, output_data);

	  for (int i = 0; i < 10; i++)
	  {
		  DBG_PRINTF("%d: %d", i, output_data[i]);
	  }

	  return 0;
}

/*
 * @brief Initializes neural net
 * @return 0 on success, negative error codes on failure
 */
int neural_net_init(void)
{
	DBG_PRINTF("initializating neural network");

	neural_net_predict_image_example();

	int i;
	for (i = 0; i < 4; i++){
		neural_net_predict(inputs[i]);
	}

	return 0;
}

