/*
 * images.h
 *
 *  Created on: Apr 4, 2018
 *      Author: A89217
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <version.h>

#include "fsl_common.h"
#include "fsl_elcdif.h"
#include "fsl_igpio.h"
#include "clock_config.h"

#include "imagelib.h"

#include "arrow_logo.h"
#include "nxp_logo.h"
#include "sample_logo.h"

#define DBG_PRINTF_PREFIX 			"imagelib> "
#define DBG_PRINTF(fmt, ...)		printk(DBG_PRINTF_PREFIX fmt "\n", ##__VA_ARGS__)


struct imagelib_img {
	uint32_t *buffer;
};

struct imagelib_img_library {
	struct imagelib_img images[IMAGELIB_IMAGE_LAST];
};

struct imagelib_img_library imglib = {
		.images = {
				{.buffer = arrow_logo_buffer},
				{.buffer = nxp_logo_buffer},
				{.buffer = sample_logo_buffer}}
};

/*
 * @brief: Copies stored image to the framebuffer
 *
 * @param img_id 		- id of the image to copy over
 * @param framebuffer	- handle to the frame buffer
 *
 * @retval 0 on success, negative error codes on failure
 */
int imagelib_draw_logo(enum imagelib_image_id img_id, uint32_t frame_buffer[APP_IMG_HEIGHT][APP_IMG_WIDTH])
{
	memcpy(frame_buffer, imglib.images[img_id].buffer, APP_IMG_HEIGHT * APP_IMG_WIDTH * sizeof(uint32_t));
	return 0;
}


